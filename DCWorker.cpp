#include "DCWorker.h"
#include <Arduino.h>

DCCheckInterface::DCCheckInterface()
{
  lastCheck = 0;
}

bool DCCheckInterface::checkPauseMs(unsigned long pauseInterval)
{
  unsigned long now = millis();
  if (now - lastCheck > pauseInterval)
  {
    lastCheck = now;
    return true;
  }
  return false;
}



DCWorker::DCWorker(int size)
{
  this->size = size;
  listWorker = new DCCheckInterface* [size];
  for (int i = 0; i < size; i++)
    listWorker[i] = 0;
}

void DCWorker::check()
{
  for (int i = 0; i < size; i++)
  {
    if (listWorker[i] == 0) break;
    listWorker[i]->check();
  }
}

int DCWorker::add(DCCheckInterface *checker)
{
  for (int i = 0; i < size; i++)
    if (listWorker[i] == 0) 
    {
      listWorker[i] = checker;
      return 0;
    }
  return 1;
}

int DCWorker::del(DCCheckInterface *checker)
{
  for(int i = 0; i < size; i++)
  {
    if (listWorker[i] == checker) {
      for (int j = i; j < size - 1; j++)
      {
        if (listWorker[j] == 0) return 0;
        listWorker[j] = listWorker[j + 1];
      }  
      listWorker[size - 1] = 0;

        return 0;
    }
  }
  return 1;
}
