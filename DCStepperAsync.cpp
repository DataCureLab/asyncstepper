#include "DCStepperAsync.h"
#include <Arduino.h>

DCStepperAsync::DCStepperAsync(int numberOfSteps, short int pinStart, DCWorker *worker) : 
  DCStepper (numberOfSteps, pinStart, pinStart+1, pinStart+2, pinStart+3)
{
  stepsToMove = 0;
  timeLock = 0;
  worker->add(this);
}

int DCStepperAsync::stop()
{
  int retSteps = stepsToMove;
  stepsToMove = 0;
  return retSteps;
}

int DCStepperAsync::getStepsToMove()
{
  return stepsToMove;
}

int DCStepperAsync::move(int stepsToMove)
{
  if (this->stepsToMove != 0 || stepsToMove == 0) return 1;
  seize();
  this->stepsToMove = stepsToMove;
  return 0;
}


void DCStepperAsync::check()
{
  unsigned long now = micros();
  
  if (stepsToMove > 0) moveInc(now);
  else if (stepsToMove < 0) moveDec(now);
  else if (checkRelease(now)) release();    
}

void DCStepperAsync::moveInc(unsigned long now)
{
  if (timeoutExpired(now)) {
    timeLock = now;
    stepsToMove--;
    DCStepper::moveInc();
    resetTimeout(now);
  }
}

void DCStepperAsync::moveDec(unsigned long now)
{
  if (timeoutExpired(now)) {
    timeLock = now;
    stepsToMove++;
    DCStepper::moveDec();
    resetTimeout(now);
  }
}

bool DCStepperAsync::checkRelease(unsigned long now)
{
  return (timeLock != 0)&&(now-timeLock >= STEPPER_RELEASE_TIMEOUT);
}

void DCStepperAsync::release()
{
  timeLock = 0;
  DCStepper::release();
}

void DCStepperAsync::seize()
{
  if (timeLock == 0) {
    DCStepper::seize();
    timeLock = micros();
    resetTimeout(timeLock);
  }
}
