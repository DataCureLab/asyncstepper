/*
 * DCStepper.h - Stepper library for Sync & Async 
 *
 *
 * The sequence of control signals for 5 phase, 5 control wires is as follows:
 *
 * Step C0 C1 C2 C3 C4
 *    1  0  1  1  0  1
 *    2  0  1  0  0  1
 *    3  0  1  0  1  1
 *    4  0  1  0  1  0
 *    5  1  1  0  1  0
 *    6  1  0  0  1  0
 *    7  1  0  1  1  0
 *    8  1  0  1  0  0
 *    9  1  0  1  0  1
 *   10  0  0  1  0  1
 *
 * The sequence of control signals for 4 control wires is as follows:
 *
 * Step C0 C1 C2 C3
 *    1  1  0  1  0
 *    2  0  1  1  0
 *    3  0  1  0  1
 *    4  1  0  0  1
 *
 * The sequence of controls signals for 2 control wires is as follows
 * (columns C1 and C2 from above):
 *
 * Step C0 C1
 *    1  0  1
 *    2  1  1
 *    3  1  0
 *    4  0  0
 *
 */

#ifndef DCStepper_h
#define DCStepper_h

// library interface description
class DCStepper {
  public:
    DCStepper(int numberOfSteps, short int pin1, short int pin2, short int pin3, short int pin4);

    void setSpeed(long whatSpeed);
    void move(int stepsToMove);
    void moveInc();
    void moveDec();
    bool timeoutExpired(unsigned long now);
    void resetTimeout(unsigned long now);

    void release();
    void seize();
  private:
    void setStep(int thisStep);

    int totalStepsNumber;      // total number of steps this motor can take
    int currentStepNumber;  // which step the motor is on

    // motor pin numbers:
    short int pin1;
    short int pin2;
    short int pin3;
    short int pin4;

    unsigned long lastStepTime;  // time stamp in us of when the last step was taken
    unsigned long stepDelayTime; // delay between steps, in ms, based on speed
};

#endif
