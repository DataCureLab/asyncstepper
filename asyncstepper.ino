#include "DCWorker.h"
#include "DCStepperAsync.h"
#include "config.h"

DCWorker worker(WORKER_POOL_SIZE);
DCStepperAsync stepper(STEPS_PER_STEPPER, START_PIN_STEPPER_CHANNEL, &worker);

void setup() {
  stepper.move(10*STEPS_PER_STEPPER);
}

void loop() {
  worker.check();
}
