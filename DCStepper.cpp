/*
 *
 * The sequence of control signals for 4 control wires is as follows:
 *
 * Step C0 C1 C2 C3
 *    1  1  0  1  0
 *    2  0  1  1  0
 *    3  0  1  0  1
 *    4  1  0  0  1
 *
 */

#include <Arduino.h>
#include "DCStepper.h"

DCStepper::DCStepper(int totalStepsNumber, short int pin1, short int pin2, short int pin3, short int pin4)
{
  this->currentStepNumber = 0;    // which step the stepper is on
  this->lastStepTime = 0; // time stamp in us of the last step taken
  this->totalStepsNumber = totalStepsNumber; // total number of steps for this stepper

  // Arduino pins for the stepper control connection:
  this->pin1 = pin1;
  this->pin2 = pin2;
  this->pin3 = pin3;
  this->pin4 = pin4;

  // setup the pins on the microcontroller:
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin4, OUTPUT);
}

/*
 * Sets the speed in revs per minute
 */
void DCStepper::setSpeed(long speed)
{
  stepDelayTime = 60L * 1000L * 1000L / totalStepsNumber / speed;
}

/*
 * Moves the stepper stepsToMove steps.  If the number is negative,
 * the stepper moves in the reverse direction.
 */
void DCStepper::move(int stepsToMove)
{
  int stepsLeft = abs(stepsToMove);  // how many steps to take
  short int direction;          // Direction of rotation

  // determine direction based on whether steps_to_mode is + or -:
  if (stepsToMove > 0) direction = 1;
  if (stepsToMove < 0) direction = 0;


  // decrement the number of steps, moving one step each time:
  while (stepsLeft > 0)
  {
    unsigned long now = micros();
    // move only if the appropriate delay has passed:
    if (timeoutExpired(now))
    {
      // get the timeStamp of when you stepped:
      resetTimeout(now);
      // increment or decrement the step number,
      // depending on direction:
      if (direction == 1)
        moveInc();
      else
        moveDec();
      // decrement the steps left:
      stepsLeft--;
    }
  }

  //Releasestepper();
}

bool DCStepper::timeoutExpired(unsigned long now)
{
  return (now - lastStepTime >= stepDelayTime);
}

void DCStepper::resetTimeout(unsigned long now)
{
  lastStepTime = now;
}

void DCStepper::moveInc()
{
  currentStepNumber++;
  if (currentStepNumber >= totalStepsNumber) currentStepNumber = 0;
  setStep(currentStepNumber % 4);
}

void DCStepper::moveDec()
{
  if (currentStepNumber <= 0) currentStepNumber = totalStepsNumber;
  currentStepNumber--;
  setStep(currentStepNumber % 4);
}

void DCStepper::seize()
{
  setStep(currentStepNumber % 4);
}

void DCStepper::release()
{
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
  digitalWrite(pin4, LOW);
}

/*
 * Moves the stepper forward or backwards.
 */
void DCStepper::setStep(int thisStep)
{
  switch (thisStep) 
  {
    case 0:  // 1010
      digitalWrite(pin1, HIGH);
      digitalWrite(pin2, LOW);
      digitalWrite(pin3, HIGH);
      digitalWrite(pin4, LOW);
    break;
    case 1:  // 0110
      digitalWrite(pin1, LOW);
      digitalWrite(pin2, HIGH);
      digitalWrite(pin3, HIGH);
      digitalWrite(pin4, LOW);
    break;
    case 2:  //0101
      digitalWrite(pin1, LOW);
      digitalWrite(pin2, HIGH);
      digitalWrite(pin3, LOW);
      digitalWrite(pin4, HIGH);
    break;
    case 3:  //1001
      digitalWrite(pin1, HIGH);
      digitalWrite(pin2, LOW);
      digitalWrite(pin3, LOW);
      digitalWrite(pin4, HIGH);
    break;
  }
}
