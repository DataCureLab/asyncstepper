#ifndef DCStepperAsync_h
#define DCStepperAsync_h

#include "DCStepper.h"
#include "DCWorker.h"

#define STEPPER_RELEASE_TIMEOUT 5000000L

class DCStepperAsync : public DCStepper, public DCCheckInterface
{
  public:
    DCStepperAsync(int numberOfSteps, short int pinStart, DCWorker *worker);
    int move(int stepsToMove);
    void check();
    void release();
    void seize();
    int stop();
    int getStepsToMove();
  private:
    void moveInc(unsigned long now);
    void moveDec(unsigned long now);
    bool checkRelease(unsigned long now);
    int stepsToMove;
    unsigned long timeLock;
};

#endif
