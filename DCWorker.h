#ifndef DCWorker_h
#define DCWorker_h



class DCCheckInterface {
  public:
    DCCheckInterface();
    virtual void check();
    bool checkPauseMs(unsigned long pauseInterval);
  private:
    unsigned long lastCheck;
};

class DCWorker {
  public:
    void check();
    DCWorker(int size);
    int add(DCCheckInterface *checker);
    int del(DCCheckInterface *checker);
  private:
    DCCheckInterface **listWorker;
    int size;
};



#endif
